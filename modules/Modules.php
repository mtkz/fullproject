<?php
    class modules  {

        function test_input($data){
            $data=trim($data);
            $data=stripcslashes($data);
            $data=htmlspecialchars($data);
            return $data;
        }


        function redirect($path){
            header("location:$path");
        }


        function initsession(){
            session_start();
        }

        function setsession($a,$b){
            $_SESSION[$a] = $b;
        }


        function stringlimit($string){
           $substring =  substr($string,0,300);

           return $substring;
        }


        function titlelimit($string){
            $substring =  substr($string,0,100);

            return $substring;
        }




    }