<?php
class Database
{

    private $db_host = "localhost";
    private $db_user = "root";
    private $db_pass = "";
    private $db_name = "fullproject";


    private $con = false;
    private $myconn = "";
    private $result = array();
    private $myQuery = "";
    private $numResults = "";


    public function __construct()
    {
        self::connecttodb();
    }



    public function connecttodb()
    {
        $this->myconn = new mysqli($this->db_host, $this->db_user, $this->db_pass, $this->db_name);
        mysqli_set_charset($this->myconn,"utf8");
        if ($this->myconn->connect_errno > 0) {
            array_push($this->result, $this->myconn->connect_error);
            return false;
        } else {
            $this->con = true;
            return true;
        }
    }



    public function insert($email,$password){
        $mysqli = $this->myconn;
        $datasql = "INSERT INTO `administrator`(`username`, `password`) VALUES (?,?)";
        $sql =$mysqli->prepare($datasql);
        $sql->bind_param("ss",$email,$password);
        if ($sql->execute()){
            return true;
        }else{
            return false;
        }
        $mysqli->close();
    }



    public function checkuser($email,$password){
        $mysqli = $this->myconn;
        $datasql = "SELECT * FROM `administrator` WHERE `username` = ? and `password` = ?";
        $sql =$mysqli->prepare($datasql);
        $sql->bind_param("ss",$email,$password);
        $sql->execute();
        $res = $sql->get_result();
        if ($res->num_rows >0){
            return true;
        }else{
            return false;
        }
        $mysqli->close();
    }



    public function checksession($username,$password){
        $mysqli = $this->myconn;
        $datasql = "SELECT * FROM `administrator` WHERE `username` = ? and `password` = ?";
        $sql =$mysqli->prepare($datasql);
        $sql->bind_param("ss",$username,$password);
        $sql->execute();
        $res = $sql->get_result();
        if ($res->num_rows >0){
            return true;
        }else{
            return false;
        }
        $mysqli->close();
    }


    public function getpost($int){
        $mysqli = $this->myconn;
        $postarray = array();
        if ($int == 1){
            $result = $mysqli->query(" SELECT * FROM `posts` WHERE `agree` = 1 ORDER BY `timestart` DESC ");
        }else{
            $result = $mysqli->query(" SELECT * FROM `posts` WHERE `agree` = 0 ORDER BY `timestart` DESC ");
        }

        while ($row = $result->fetch_assoc()){
            $postarray[] = $row;
        }
        return $postarray;

        $mysqli->close();
    }

    public function deletepost($id){
        $mysqli = $this->myconn;
        $datasql = "DELETE FROM `posts` WHERE id = ?";
        $sql =$mysqli->prepare($datasql);
        $sql->bind_param("i",$id);
        $sql->execute();

        $mysqli->close();
    }


    public  function updatepost($id){
        $mysqli = $this->myconn;
        $datasql = "UPDATE `posts` SET `agree` = 1 WHERE `id` = ?";
        $sql =$mysqli->prepare($datasql);
        $sql->bind_param("i",$id);
        $sql->execute();

        $mysqli->close();
    }




}





