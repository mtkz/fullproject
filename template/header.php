<?php
    include dirname(__DIR__) . DIRECTORY_SEPARATOR . "modules/path.php";
?>
<!DOCTYPE html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <title>FullProject</title>
    <link rel="stylesheet" href="<?php echo SITE_ADDRESS ?>css/style.css">
    <link rel='stylesheet prefetch' href='<?php echo SITE_ADDRESS ?>css/animate.min.css'>
</head>
<body>
<div class="mt_top_menu ">
    <ul class="wow slideInRight"">
        <div>
            <a href="">دانلود application</a>
        </div>
        <li><a href="">خانه</a></li>
        <li><a href="">درباره ما</a></li>
        <li><a href="">تماس با ما</a></li>
        <li><a href="<?php echo LOGIN_ADDRESS ?>">ورود</a></li>
        <li><a href="">درخواست تبلیغات</a></li>
    </ul>
</div>