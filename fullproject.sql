-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 30, 2018 at 06:52 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fullproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

CREATE TABLE `administrator` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `administrator`
--

INSERT INTO `administrator` (`id`, `username`, `password`) VALUES
(1, 'admin', '60fae477c27a6295523ef8532870eb8e');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `desc` text NOT NULL,
  `timestart` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `timeleft` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `agree` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `desc`, `timestart`, `timeleft`, `agree`) VALUES
(21, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:11', '0000-00-00 00:00:00', 0),
(25, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:11', '0000-00-00 00:00:00', 0),
(27, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:11', '0000-00-00 00:00:00', 0),
(30, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:11', '0000-00-00 00:00:00', 0),
(37, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-29 16:31:47', '0000-00-00 00:00:00', 1),
(38, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-29 16:31:52', '0000-00-00 00:00:00', 1),
(39, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:12', '0000-00-00 00:00:00', 0),
(41, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:12', '0000-00-00 00:00:00', 0),
(42, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:12', '0000-00-00 00:00:00', 0),
(44, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:12', '0000-00-00 00:00:00', 0),
(45, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:12', '0000-00-00 00:00:00', 0),
(46, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:12', '0000-00-00 00:00:00', 0),
(47, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:12', '0000-00-00 00:00:00', 0),
(48, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:12', '0000-00-00 00:00:00', 0),
(49, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:12', '0000-00-00 00:00:00', 0),
(50, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:12', '0000-00-00 00:00:00', 0),
(51, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:12', '0000-00-00 00:00:00', 0),
(52, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:12', '0000-00-00 00:00:00', 0),
(53, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:12', '0000-00-00 00:00:00', 0),
(54, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:12', '0000-00-00 00:00:00', 0),
(55, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:12', '0000-00-00 00:00:00', 0),
(56, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:12', '0000-00-00 00:00:00', 0),
(57, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:12', '0000-00-00 00:00:00', 0),
(58, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:12', '0000-00-00 00:00:00', 0),
(59, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:12', '0000-00-00 00:00:00', 0),
(60, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:12', '0000-00-00 00:00:00', 0),
(61, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:12', '0000-00-00 00:00:00', 0),
(62, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:12', '0000-00-00 00:00:00', 0),
(63, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:12', '0000-00-00 00:00:00', 0),
(64, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:12', '0000-00-00 00:00:00', 0),
(65, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:12', '0000-00-00 00:00:00', 0),
(66, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:12', '0000-00-00 00:00:00', 0),
(67, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:12', '0000-00-00 00:00:00', 0),
(68, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:12', '0000-00-00 00:00:00', 0),
(69, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:12', '0000-00-00 00:00:00', 0),
(70, 'test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2018-07-28 09:42:12', '0000-00-00 00:00:00', 0),
(76, 'لورم ', 'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد', '2018-07-28 13:07:23', '0000-00-00 00:00:00', 1),
(77, 'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد', 'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد', '2018-07-28 13:07:11', '0000-00-00 00:00:00', 1),
(78, ' لورم', 'تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 ', '2018-07-28 12:24:36', '0000-00-00 00:00:00', 1),
(81, ' لورم', 'تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 ', '2018-07-28 12:24:45', '0000-00-00 00:00:00', 1),
(82, ' لورم', 'تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 ', '2018-07-28 12:24:45', '0000-00-00 00:00:00', 1),
(83, ' لورم', 'تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 ', '2018-07-28 12:24:45', '0000-00-00 00:00:00', 1),
(84, ' لورم', 'تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 ', '2018-07-28 12:24:45', '0000-00-00 00:00:00', 1),
(85, ' لورم', 'تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 ', '2018-07-28 12:24:45', '0000-00-00 00:00:00', 1),
(86, ' لورم', 'تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 ', '2018-07-28 12:24:45', '0000-00-00 00:00:00', 1),
(87, ' لورم', 'تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 ', '2018-07-28 12:24:45', '0000-00-00 00:00:00', 1),
(88, ' لورم', 'تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 تست 1 ', '2018-07-28 12:24:45', '0000-00-00 00:00:00', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrator`
--
ALTER TABLE `administrator`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `administrator`
--
ALTER TABLE `administrator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
