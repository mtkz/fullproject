<?php include  __DIR__ . DIRECTORY_SEPARATOR . "admintemplate/header.php";?>
<?php include  __DIR__ . DIRECTORY_SEPARATOR . "admintemplate/adminmenu.php";?>
<div class="mt_main">
    <p class="mt_admin_messages"></p>

    <table>
        <tr class="mt_table_head">
            <td>عنوان</td>
            <td>متن</td>
            <td>زمان انتشار</td>
            <td>حذف</td>
        </tr>
        <?php
        $a= $database->getpost(1);
        foreach ($a  as $key => $res){
            $desc = $modules->stringlimit($res['desc']);
            $title = $modules->titlelimit($res['title']);
            echo "<tr>";
            echo"<td class='title'>$title</td>";
            echo "<td>$desc</td>";
            echo"<td>$res[timestart]</td>";
            echo"<td  id='$res[id]' class='trash'>X</td>";
            echo "</tr>";
        }
        ?>

    </table>

</div>
<script src="<?php echo ADMIN_ADDRESS ?>js/jQuery.js"></script>
<script>
    $(document).ready(function () {
        var delid = $('.trash').attr('id')
        var delidpro = 'td#' + delid
        $('.trash').click(function (e) {
            $.ajax({
                type:'POST',
                url:'adminmodules/adminmodules.php',
                data:{action:'postdelete',delid:delid},
                success:function (response) {
                    $(delidpro).parent().fadeOut();
                    $('.mt_admin_messages').fadeIn().html(response).delay(2000).fadeOut();
                }
            })
            e.preventDefault();
        })
    })

</script>
<?php include  __DIR__ . DIRECTORY_SEPARATOR . "admintemplate/footer.php";?>